"""
Načtěte od uživatele poloměr
kruhu a vypište jeho obvod/obsah
"""
polomer = int(input("Zadej polomer:"))
obvod = round(2*3.14*polomer, 1)
obsah = round(3.14 * polomer**2, 1)
print(f"Obvod je {obvod}")
print(f"Obsah je {obsah}")

