"""
Napište program, který v zadaném stringu zamění
všechny výskyty prvního znaku za $,
první znak však zůstane zachován
např. restart -> resta$t, pepa -> pe$a
"""
slovo = "pepa"
slovo = slovo[0] + slovo[1:].replace(slovo[0],"$")
print(slovo)