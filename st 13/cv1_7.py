#  Vypište každý druhý znak vašeho jména
#  pepa -> pp
jmeno = "Mikulas"
print(jmeno[::2])

#  Vypište vypište vaše jméno opačně.
#  pepa -> apep
print(jmeno[::-1])