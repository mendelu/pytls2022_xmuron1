"""
Do proměnné pi si uložte číslo 3.1415926
a vypište je pouze na dvě desetinná místa
pomocí f-strings
"""
pi = 3.1415926
print(f"{pi:.2f}")
"""
Zarovnejte string uložený v proměnné, takovým 
způsobem, že bude vždy 20 znaků 
dlouhý a hodnota proměnné bude uprostřed. 
Prázdné místa budou vyplněna hvězdičkami
"""
retezec = "Mikulas"
print(f"{retezec:*^20}")
# ^ - na sted
# <> - doleva doprava

"""
Vyvtřte následující tabulku
Předmět-------------Známka----Počet kreditů----
ZOO--------------------A------------5----------
ALGO-------------------B------------6----------
TZI—-------------------C------------5----------
"""

print(f"{'Předmět':-<20}{'Známka':-^10}{'Počet kreditů':-^20}")
print(f"{'ZOO' :-<20}{'A':-^10}{5:-^20}")
print(f"{'ALG' :-<20}{'F':-^10}{6:-^20}")