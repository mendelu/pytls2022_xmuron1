"""
Načtěte z konzole pomocí input jméno
uživatele a vypište jeho délku (počet znaků)
"""
jmeno = input("Zadej jmeno: ")
print(f"Jmeno {jmeno} ma delku {len(jmeno)}")

"""
Upravte předchozí příklad tak, 
aby bylo jméno vždy vypsáno prvním 
velkým písmenem
"""
jmeno = input("Zadej jmeno: ")
print(f"Jmeno {jmeno.capitalize()} ma delku {len(jmeno)}")

"""
upravte předchozí příklad tak, 
aby bylo jméno vždy vypsáno celé kapitálkama 
"""

jmeno = input("Zadej jmeno: ")
print(f"Jmeno {jmeno.upper()} ma delku {len(jmeno)}")


