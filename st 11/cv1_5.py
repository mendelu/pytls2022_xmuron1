"""
Napište program, který v zadaném stringu
zamění všechny výskyty prvního znaku za $,
první znak však zůstane zachován
např. restart -> resta$t, pepa -> pe$a
"""
vstup = "pepa"
print(vstup[0]+vstup[1:].replace(vstup[0],"$"))