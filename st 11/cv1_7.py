"""
Vypište každý druhý znak vašeho jména
Mikuláš -> Mklš
"""

jmeno = "Mikulás"
# jmeno[od:do:krok]
print(jmeno[::2])

"""
Načtěte od uživatele jeho jméno a vypište 
jej opačně. 
Mikulas -> salukiM
"""
jmeno = "Mikulás"
print(jmeno[::-1])
