"""
Načtěte od uživatele poloměr
kruhu a vypište jeho obvod/obsah
"""

polomer = input("Zadej polomer:\n")
polomer = int(polomer)

obvod = round(2 * 3.14 * polomer)
obsah = round(3.14 * (polomer ** 2))

print(f"Obvod: {obvod}")
print(f"Obsah: {obsah}")
