"""
Načtěte z konzole pomocí input jméno uživatele
a vypište jeho délku (kolik má znaků)
"""

jmeno = input("Jak se jmenujes?")
print(f"Jmeno {jmeno} ma delku {len(jmeno)}")

"""
Upravte předchozí příklad tak, 
aby bylo jméno vždy vypsáno celé kapitálkama 
"""
print(f"Jmeno upper je {jmeno.upper()}")
print(f"Jmeno capitalize je {jmeno.capitalize()}")



